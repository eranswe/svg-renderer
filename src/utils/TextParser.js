
const regexpComments = new RegExp("(\\/\\/.*$)|(\\/\\*([^\\*\\\\]|\\n)*\\*\\/)")
const regexpCluster = new RegExp("(package |group |cluster |start )([^:]*)[:]?(.*)")
const regexpClusterEnd = new RegExp("(^/|^/package|^/cluster|^end)")
const regexpNode = new RegExp("(class |interface |node )([^:]*)[:]?(.*)")
const regexpExtends = new RegExp("(extends |implements )(.*)")
const regexpAssosiation = new RegExp("(\\+ |public |\\# |protected |\\- |private )?([^ \\[\\]<>]*)(\\[\\])?(<.*>)? (.*)")

export function parse(text) {

    const model = { nodes: {}, edges: [] }

    let currentNode = undefined;
    let parentNodes = [];

    const rows = text.split("\n");

    rows.forEach((r) => {

        r = r.replace("\\s+", " ").replace(regexpComments, "1-$1, 2-$2").trim();

        if (regexpClusterEnd.test(r)) {
            parentNodes.pop();
            currentNode = undefined;
            return;
        }

        const clusterGroups = regexpCluster.exec(r);
        if (clusterGroups !== null) {
            parentNodes.push(resolveNode(model, {
                type: clusterGroups[1].trim(),
                id: clusterGroups[2].trim(),
                label: clusterGroups[3].trim(),
                parent: parentNodes.length > 0 ? parentNodes[parentNodes.length - 1].id : undefined
            }))
            return;
        }

        const extentionGroups = regexpExtends.exec(r);
        if (extentionGroups !== null && currentNode !== undefined) {
            const toNode = resolveNode(model, {
                id: extentionGroups[2].trim()
            })
            model.edges.push({
                from: currentNode.id,
                to: toNode.id,
                type: extentionGroups[1].trim()
            });
            return;
        }

        const nodeGroups = regexpNode.exec(r);
        if (nodeGroups !== null) {
            currentNode = resolveNode(model, {
                type: nodeGroups[1].trim(),
                id: nodeGroups[2].trim(),
                label: nodeGroups[3].trim(),
                parent: parentNodes.length > 0 ? parentNodes[parentNodes.length - 1].id : undefined
            })
            return;
        }

        if (currentNode === undefined && r.length > 0) {
            currentNode = resolveNode(model, {
                id: r,
                parent: parentNodes.length > 0 ? parentNodes[parentNodes.length - 1].id : undefined
            })
            return;
        }

        if (r.length > 0) {
            r.replace("public ", "+ ");
            r.replace("protected ", "# ");
            r.replace("private ", "- ");
            if (r.lastIndexOf(")") > r.indexOf("(")) {
                currentNode.operations.push(r);
            } else {
                currentNode.properties.push(r);
            }
        }

    })

    for (const nodeId in model.nodes) {
        const fromNode = model.nodes[nodeId];
        fromNode.properties.forEach((prop) => {
            const assosiationGroups = regexpAssosiation.exec(prop);
            if (assosiationGroups !== null) {
                const toNode1 = model.nodes[assosiationGroups[2].trim()];
                if (toNode1 !== undefined) {
                    model.edges.push({
                        from: fromNode.id,
                        to: toNode1.id,
                        type: assosiationGroups[3] === "[]" ? "aggregate" : "assosiation"
                    });
                }
                const str = assosiationGroups[4] === undefined
                    ? [] : assosiationGroups[4].replace("<", "").replace(">", ",").split(",");
                str.forEach(s => {
                    if (s.length > 0) {
                        const toNode2 = model.nodes[s.trim()];
                        if (toNode2 !== undefined) {
                            model.edges.push({
                                from: fromNode.id,
                                to: toNode2.id,
                                type: "composition"
                            });
                        }
                    }
                })
                return;
            }
        });
    }
    console.log("text parsed: ", model);
    return model;
}

function resolveNode(model, node) {
    const existingNode = model.nodes[node.id];
    if (existingNode === undefined) {
        if (node.type === undefined || node.type.length == 0) {
            node.type = "node";
        }
        if (node.label === undefined || node.label.length == 0) {
            node.label = node.id;
        }
        if (node.propertiers === undefined) {
            node.properties = [];
        }
        if (node.operations === undefined) {
            node.operations = [];
        }
        model.nodes[node.id] = node;
        return node;
    } else {
        if (node.type !== undefined) existingNode.type = node.type;
        if (node.label !== undefined) existingNode.label = node.label;
        if (node.parent !== undefined) existingNode.parent = node.parent;
        if (node.properties !== undefined) {
            existingNode.properties.push.apply(
                existingNode.properties, node.propeties);
        }
        if (node.operations !== undefined) {
            existingNode.operations.push.apply(
                existingNode.operations, node.operations);
        }
        if (existingNode.label === undefined || existingNode.label.length == 0) {
            existingNode.label = existingNode.id;
        }
        return existingNode;
    }
}
