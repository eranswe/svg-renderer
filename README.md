# svg-renderer

node, class, interface
- class ClassName
- class c : ClassName

(class |interface |node )?([^:]*)[:]?(.*)

extends, implements
- extends ClassName
- implements c

(extends |implements )(.*)

assosiation

public/protected/private
+/#/-

(public |protected |private )?([^ ]*) (.*)
(public |protected |private )?([^ \[\]]*)(\[\])? (.*)
(public |protected |private )?([^ \[\]<>]*)(\[\])?(<.*>)? (.*)

ClassName className
ClassName[] classNameArray
ListClassName<ClassNameA, ClassNameB> classNameList

package package.name
package p : package.name

(package |start |group |cluster )([^:]*)[:]?(.*)



## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```
Go to [http://localhost:8080](http://localhost:8080)

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
